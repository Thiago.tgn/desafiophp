<?php

namespace App\Entity;

use App\Repository\EmpresaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EmpresaRepository::class)]
class Empresa
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $nomeempresa = null;

    #[ORM\ManyToMany(targetEntity: Socio::class, inversedBy: 'idempresa')]
    private Collection $idSocio;

    public function __construct()
    {
        $this->idSocio = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomeempresa(): ?string
    {
        return $this->nomeempresa;
    }

    public function setNomeempresa(string $nomeempresa): static
    {
        $this->nomeempresa = $nomeempresa;

        return $this;
    }

    /**
     * @return Collection<int, Socio>
     */
    public function getIdSocio(): Collection
    {
        return $this->idSocio;
    }

    public function addIdSocio(Socio $idSocio): static
    {
        if (!$this->idSocio->contains($idSocio)) {
            $this->idSocio->add($idSocio);
        }

        return $this;
    }

    public function removeIdSocio(Socio $idSocio): static
    {
        $this->idSocio->removeElement($idSocio);

        return $this;
    }
}
