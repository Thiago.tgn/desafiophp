<?php

namespace App\Entity;

use App\Repository\SocioRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SocioRepository::class)]
class Socio
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $nome = null;

    #[ORM\ManyToMany(targetEntity: Empresa::class, mappedBy: 'idSocio')]
    private Collection $idempresa;

    public function __construct()
    {
        $this->idempresa = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNome(): ?string
    {
        return $this->nome;
    }

    public function setNome(string $nome): static
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * @return Collection<int, Empresa>
     */
    public function getIdempresa(): Collection
    {
        return $this->idempresa;
    }

    public function addIdempresa(Empresa $idempresa): static
    {
        if (!$this->idempresa->contains($idempresa)) {
            $this->idempresa->add($idempresa);
            $idempresa->addIdSocio($this);
        }

        return $this;
    }

    public function removeIdempresa(Empresa $idempresa): static
    {
        if ($this->idempresa->removeElement($idempresa)) {
            $idempresa->removeIdSocio($this);
        }

        return $this;
    }
}
