<?php
namespace App\Controller;

use App\Entity\Empresa;
use App\Entity\Socio;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EmpresaController extends AbstractController
{

    public function index(EntityManagerInterface $em): Response
{
    $empresas = $em->getRepository(Empresa::class)->findAll();
    $info = [];
    
    // Percorra os objetos da entidade Empresa e extraia os dados relevantes
    foreach ($empresas as $empresa) {
        $info[] = [
            'id' => $empresa->getId(),
            'nome' => $empresa->getNomeempresa(),
            // Adicione mais campos conforme necessário
        ];
    }

    return $this->json(['data' => $info], 200);
    }

    public function SocioEmpresa(EntityManagerInterface $em, Request $request,$id): Response
    {
        $idEmpresa = $request->request->get('idEmpresa'); // Assuming 'nome' is the parameter name

        $socio = $em->getRepository(Socio::class)->find($id);
        $socio->addIdempresa($idEmpresa);

        try {
            $em->persist($socio);
            $em->flush();

            $msg = "Empresa salvo com sucesso";
        } catch (Exception $e) {
            $msg = "Erro ao salvar a Empresa";
        }

        return $this->json(['mensagem' => $msg]);
    }


    public function store(EntityManagerInterface $em, Request $request): Response
    {
        $nome = $request->request->get('nome'); // Assuming 'nome' is the parameter name


       
        if (!isset($nome)) {
            return new JsonResponse(['mensagem' => 'Nome do Socio não fornecido'], 400);
        }
    
    
        

        $empresa = new Empresa();
        $empresa->setNomeempresa($nome);

        try {
            $em->persist($empresa);
            $em->flush();

            $msg = "Empresa salvo com sucesso";
        } catch (Exception $e) {
            $msg = "Erro ao salvar o Empresa";
        }

        return $this->json(['mensagem' => $msg],201);
    }

    public function put(EntityManagerInterface $em, Request $request,$id): Response
    {
        $nome = $request->request->get('nome'); // Assuming 'nome' is the parameter name

        $empresa = $em->getRepository(Empresa::class)->find($id);
        $empresa->setNomeempresa($nome);

        try {
            $em->persist($empresa);
            $em->flush();

            $msg = "Empresa salvo com sucesso";
        } catch (Exception $e) {
            $msg = "Erro ao salvar a Empresa";
        }

        return $this->json(['mensagem' => $msg]);
    }

    public function DELETE(EntityManagerInterface $em, Request $request,$id): Response
    {

        $socio = $em->getRepository(Empresa::class)->find($id);

        try {
            $em->remove($socio);
            $em->flush();

            $msg = "removido a Empresa com sucesso";
        } catch (Exception $e) {
            $msg = "Erro ao salvar o Empresa";
        }

        return $this->json(['mensagem' => $msg]);
    }
}