<?php
namespace App\Controller;

use App\Entity\Socio;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SocioController extends AbstractController
{

    public function index(EntityManagerInterface $em): Response
    {
        $socios = $em->getRepository(Socio::class)->findAll();
        $info = [];
    
        // Percorra os objetos da entidade Empresa e extraia os dados relevantes
        foreach ($socios as $socio) {
            $info[] = [
                'id' => $socio->getId(),
                'nome' => $socio->getNome(),
                // Adicione mais campos conforme necessário
            ];
        }
        return $this->json(['data' => $info]);
    }

    public function store(EntityManagerInterface $em, Request $request): Response
    {
        $nome = $request->request->get('nome'); // Assuming 'nome' is the parameter name


       
        if (!isset($nome)) {
            return new JsonResponse(['mensagem' => 'Nome do Socio não fornecido'], 400);
        }
    
    
        

        $socio = new Socio();
        $socio->setNome($nome);

        try {
            $em->persist($socio);
            $em->flush();

            $msg = "Socio salvo com sucesso";
        } catch (Exception $e) {
            $msg = "Erro ao salvar o Socio";
        }

        return $this->json(['mensagem' => $msg],201);
    }

    public function put(EntityManagerInterface $em, Request $request,$id): Response
    {
        $nome = $request->request->get('nome'); // Assuming 'nome' is the parameter name

        $socio = $em->getRepository(Socio::class)->find($id);
        $socio->setNome($nome);

        try {
            $em->persist($socio);
            $em->flush();

            $msg = "Socio salvo com sucesso";
        } catch (Exception $e) {
            $msg = "Erro ao salvar o Socio";
        }

        return $this->json(['mensagem' => $msg]);
    }

    public function DELETE(EntityManagerInterface $em, Request $request,$id): Response
    {

        $socio = $em->getRepository(Socio::class)->find($id);

        try {
            $em->remove($socio);
            $em->flush();

            $msg = "removido o socio com sucesso";
        } catch (Exception $e) {
            $msg = "Erro ao salvar o Socio";
        }

        return $this->json(['mensagem' => $msg]);
    }
}