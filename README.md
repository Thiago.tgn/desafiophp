
# Genrenciador de socia com empresas

Esse software vem com o objetivo de gerenciar os socios e as empresas que tem algun vinculo entre-se, 

## tecnologias

PHP 8.2.10-2ubuntu1 \
Composer 2.7.1 \
Symfony  5.8.11
## Documentação da API

### Parte Voltada aos socios

##### retorna uma lista de todos os socios:

```http
  GET /Socio
```

| Parâmetro   | Tipo       | Descrição                           |
| :---------- | :--------- | :---------------------------------- |
| `nome` | `string` | **Obrigatório**. nome do socio |

##### Criar um socio

```http
  POST /Socio/create
```
passar ->
| Parâmetro   | Tipo       | Descrição                                   |
| :---------- | :--------- | :------------------------------------------ |
| `nome` | `string` | **Obrigatório**. nome do socio |

##### Upgrade do socio

----

```http
  PUT /Socio/update/{id}
```

| Parâmetro   | Tipo       | Descrição                                   |
| :---------- | :--------- | :------------------------------------------ |
| `nome` | `string` | **Obrigatório**. nome do socio |


##### Delete do socio

```http
  DELETE  /Socio/delete/{id}
```




### Parte Voltada aos Empresa
##### retorna uma lista de todos os Empresa:

```http
  GET /Empresa
```

| Parâmetro   | Tipo       | Descrição                           |
| :---------- | :--------- | :---------------------------------- |
| `nome` | `string` | **Obrigatório**. nome da Empresa |

##### Criar um Empresa

```http
  POST /Empresa/create
```
passar ->
| Parâmetro   | Tipo       | Descrição                                   |
| :---------- | :--------- | :------------------------------------------ |
| `nome` | `string` | **Obrigatório**. nome da Empresa |

##### Upgrade da Empresa

----

```http
  PUT /Empresa/update/{id}
```

| Parâmetro   | Tipo       | Descrição                                   |
| :---------- | :--------- | :------------------------------------------ |
| `nome` | `string` | **Obrigatório**. nome da Empresa |


##### Delete da Empresa

```http
  DELETE  /Empresa/delete/{id}
```


## Instalação e rodar

Instalar o desafio com composer e symfony

```bash
  composer install
  symfony server:start
```
    